
import os
import sys
import time
import pdb
import glob
from datetime import datetime, timedelta
from dateutil import parser
from time import gmtime, strftime
from datetime import datetime

import message
import user_class

class Controller:
    
    

    def __init__(self, in_pathToUsers, in_pathToInbox, in_delay, in_senderGetsHisOwnMessage, in_pullmessageKeywordArray, in_pullmessageAnswerArray, in_pullmessageDefaultAnswer, in_partyMode, in_sendTimestamp, in_helpText, in_ownNumber, in_timeBetweenSelfTests, in_treshholdToInformAdmin, in_numberOfAdmin ):
        self.pathToUsers = ""
        self.pathToInbox = ""
        self.delay = -1
        self.senderGetsHisOwnMessage = in_senderGetsHisOwnMessage
        self.partialReplacementOfSpecialCharacters = True
        self.pullmessageKeywordArray = in_pullmessageKeywordArray
        self.pullmessageAnswerArray = in_pullmessageAnswerArray
        self.pullmessageDefaultAnswer = in_pullmessageDefaultAnswer
        self.partyMode = in_partyMode
        self.sendTimeStamp = in_sendTimestamp
        self.helpText = in_helpText
        self.forbiddenChannelnames = [ "selftest", "ping", "pong", "list", "hallo", "hello", "channel", "channels", "mute", "unmute", "exit", "join" ]
        self.numberOfAdmin = in_numberOfAdmin
        
        self.ownNumber = in_ownNumber           #the number of the sms-distrubutor itself
        self.timeLastSelfTestHasBeenSent = datetime.now()
        time.sleep( 1 )
        self.timeLastSelfTestHasBeenReceived = datetime.now()
        self.timeBetweenSelfTests = int( in_timeBetweenSelfTests )
        self.selftestCounter = 0
        self.receivedSelftestsArray = []
        self.treshholdToInformAdmin = int( in_treshholdToInformAdmin )
        self.adminIsInformed = False
        
        self.sendSelfTest()
        
        if in_pathToInbox[-1:] != "/":
            in_pathToInbox = in_pathToInbox + "/"
        if in_pathToUsers[-1:] != "/":
            in_pathToUsers = in_pathToUsers + "/"
        self.pathToUsers = in_pathToUsers
        self.pathToInbox = in_pathToInbox
        self.delay = in_delay
        print("inbox path: " + self.pathToInbox)
        print("user path: " + self.pathToUsers)
        print("")
        while 42 == 42:         #check for new messages:
            time.sleep( int(self.delay) )
            #print("checking")
            completePath = self.pathToInbox[:-1] + "/*.txt"
            pathesToIncommingMessages = False
            pathesToIncommingMessages = glob.glob( completePath )   #gets all files in the inbox-folder 
            #pdb.set_trace()
            ArrayWithMessages = []
            if len(pathesToIncommingMessages) >= 1:                 #are there any files?
                ArrayWithMessages = self.getArrayWithMessages( pathesToIncommingMessages )  #is is very likely that messages, that are longer then the standard-sms-size will be split into multiple "small" messages. this function merges them back together to a single one
            for incommingMessage in ArrayWithMessages:
                print( "received a message: " + str(incommingMessage.path) )
                #print( "content: " + str(incommingMessage.content) )
                numberOfSender = incommingMessage.fromNumber
                #pdb.set_trace()
                if incommingMessage.isPullMessageRequest == False:
                    self.interpretMessage( incommingMessage )
                else:
                    print( "got pullMessageRequest from number: " + str(incommingMessage.fromNumber) + str(" ; answerIndex: ") + str(incommingMessage.answerIndex) )
                    self.handlePullMessageRequest( incommingMessage )
            pass
            #now the selftesting part:
            if float( (datetime.now() - self.timeLastSelfTestHasBeenSent).total_seconds() ) > self.timeBetweenSelfTests:
                #print("time to do a self-test")
                #self.selftestCounter += 1
                #toSendString =  'echo "@@selftest.' + str(self.selftestCounter) + '" | gammu-smsd-inject TEXT ' + self.ownNumber + ' -len 10 &>/dev/null'
                self.sendSelfTest()
            else:
                pass
                #print("to early for a self-test")
            
            delta = float( (datetime.now() - self.timeLastSelfTestHasBeenReceived).total_seconds() )
            #print("delta: " + str(delta))
            if delta > self.treshholdToInformAdmin:
                print("delta > self.treshholdToInformAdmin: delta: " + str(delta))
                if self.adminIsInformed == False:
                    print("inform admin!!!!")
                    self.informAdmin()
                    self.adminIsInformed = True # we only want the admin informed once!


    def sendSelfTest( self ):
        toSendString =  'echo "@@selftest.' + str(self.selftestCounter) + '" | gammu-smsd-inject TEXT ' + self.ownNumber + ' -len 10'
        print( "toSendString: " + str(toSendString) )
        os.system( toSendString )
        self.timeLastSelfTestHasBeenSent = datetime.now()
        self.selftestCounter += 1

    def informAdmin( self ):
        toSendString =  'echo "selftest failed!!" | gammu-smsd-inject TEXT ' + self.numberOfAdmin + ' -len 17'
        print( "toSendString: " + str(toSendString) )
        os.system( toSendString )


    def getArrayWithMessages(self, in_pathesToIncommingMessages):
        # first we need to check if there are multiple-messages at all. 
        # a multiple-message can be recognized by the file-name. a single-message 
        # has the format: IN20160514_110957_00_+49123456798_00.txt
        # where             ^ this is the date
        #                            ^ this is the time of receive.
        #          i do not know what this ^ is
        #                              but this ^ is the sender's number
        #                                          and this ^ is a counter, which indicates the 'position' in a multi-message.
        # so, first we check for this numbers.
        arrayWithMessages = []
        answerIndex = -1
        #pdb.set_trace()
        for singlePath in in_pathesToIncommingMessages:
            fileName = singlePath.split("/").pop()  #file name of the message
            fileName = fileName.split(".")[0]       #without the '.'-extention
            splitted = fileName.split("_")          # zeroes element contains date, first the time, second i dont know, third the number of sender, fourth the position in a multi-message
            SMS_date = False
            SMS_time = False
            fromNumber = False
            positionInMultiMessage = False
            try:
                SMS_date = splitted[0][2:]              #date without the 'IN'
                SMS_time = splitted[1]
                fromNumber = splitted[3]
                positionInMultiMessage = int( splitted[4] )
            except:
                print("unknown filename-format")    #something went wrong
                self.shutdown()
            
            #that^ was the header,
            #now comes the content:            
            textFile = open(singlePath, 'rb')               # i need to open it as a byte-object to decode it as utf-8 and replace non-valid characters with the '?'-symbol. i learned that at the 33c3
            text = textFile.read()
            text = text.decode("utf-8", errors="replace")
            
            text = text.lower()                                     #everything to lower case. it could cause problems, when channel names are not spelled correctly (upper/lower-case)
            if self.partialReplacementOfSpecialCharacters == True:
                text = text.replace("ä", "ae")                      #gammu has problems to send utf-8 characters. i was not able to fix that. so i just replace the characters with ascii-conform ones
                text = text.replace("ö", "oe")
                text = text.replace("ü", "ue")
                text = text.replace("ß", "ss")
                text = text.replace('"', "'")
            if text[-1:] == "\n":
                text = text[:-1]            #removing "/n" if there (happens at debugging)
            print("\n\nmessage received: text: " + text)
            isPullMessageRequest = False
            
            #for index in range(0, len(self.pullmessageKeywordArray)):
            #    pdb.set_trace()
            #    if text in self.pullmessageKeywordArray[index]:
            #        answerIndex = index
            #        isPullMessageRequest = True  
                    
            
            #pdb.set_trace()
            answerIndex = 0
            found = False
            for keyWordSubArray in self.pullmessageKeywordArray:
                if found == False:
                    for keyWord in keyWordSubArray:
                        if keyWord in text and found == False:
                            answerIndex += 1
                            isPullMessageRequest = True  
                            found = True
                            break
            
            arrayWithMessages.append( message.Message( singlePath, SMS_date, SMS_time, fromNumber, positionInMultiMessage, text, isPullMessageRequest, answerIndex-1) )
            print("removing file: " + singlePath)
            os.remove(singlePath) #removing the incomming message
        
        #pdb.set_trace()
        #debuging:
        #for element in arrayWithMessages:
        #    print(element.content)
        #    print("\n")
        #
        
        
        #so now we have an array with message-objects. but the multi-messages are still separated. means: we got a bunch of small messages, which need to be merged
        sorted_arrayWithMessages_single_1D = sorted( arrayWithMessages, key = lambda x: x.positionInMultiMessage, reverse = True )
        #the 0th element in this array should now have the highest positionInMultiMessage
        
        #pdb.set_trace()
        #debuging:
        #for element in sorted_arrayWithMessages_single_1D:
        #    print("element.content:")
        #    print(element.content)
        #    print("element.positionInMultiMessage:")
        #    print(element.positionInMultiMessage)
        #    print("\n")
        ###
                
        highestPositionInMultiMessage = -1
        for singleMessage in sorted_arrayWithMessages_single_1D:
            if singleMessage.positionInMultiMessage > highestPositionInMultiMessage:
                highestPositionInMultiMessage = singleMessage.positionInMultiMessage
        #pdb.set_trace()
        sorted_arrayWithMessages_single_2D = []
        for index_2D in range( 0, highestPositionInMultiMessage+1): ##the plus one, because the hightestPosition means, the highest occuring number. then we have to add 1 to reach that highest one!!
            tempArray = []
            indexesToDelete = []
            #print( "len(sorted_arrayWithMessages_single_1D): " + str( len(sorted_arrayWithMessages_single_1D) ) )
            for index_1D in range( 0, len(sorted_arrayWithMessages_single_1D) ):
                #print( "index_1D: " + str( index_1D ) )
                if sorted_arrayWithMessages_single_1D[index_1D].positionInMultiMessage == index_2D:
                    #print( "sorted_arrayWithMessages_single_1D[index_1D].positionInMultiMessage == index_2D" )
                    #print( "index_1D: " + str( index_1D ) )
                    tempArray.append( sorted_arrayWithMessages_single_1D[index_1D] )
                    indexesToDelete.append( index_1D )
            #indexesToDelete = sorted( indexesToDelete, key = lambda x: x.positionInMultiMessage, reverse = True )
            for index in reversed(indexesToDelete):
                #print("deleting index " + str(index))
                sorted_arrayWithMessages_single_1D.pop( index )
            sorted_arrayWithMessages_single_2D.append(tempArray)
            
        #in the 2D-array we have now all messages: in the first dimension with decreasing positionInMultiMessage-index. in the second dimension are all indexes the same.
        # the problem: we still need to sort this second-dimension-arrays by it's arrival-time
        
        
        ###debugging
        #print("before sorting:")
        #for index0 in range( 0, len(sorted_arrayWithMessages_single_2D) ):
        #    for index1 in range( 0, len(sorted_arrayWithMessages_single_2D[index0] ) ):
        #        print( "sorted_arrayWithMessages_single_2D[" + str(index0) + "][" + str(index1) + "].positionInMultiMessage: " + str( sorted_arrayWithMessages_single_2D[index0][index1].positionInMultiMessage ) )
        #        print( "sorted_arrayWithMessages_single_2D[" + str(index0) + "][" + str(index1) + "].time: " + str( sorted_arrayWithMessages_single_2D[index0][index1].time ) )
        ###debugging
        
        
        
        for index in range( 0, highestPositionInMultiMessage+1):
            sorted_arrayWithMessages_single_2D[index] = sorted( sorted_arrayWithMessages_single_2D[index], key = lambda x: x.time, reverse = False )
        
        
        ###debugging
        #print("after sorting:")
        #for index0 in range( 0, len(sorted_arrayWithMessages_single_2D) ):
        #    for index1 in range( 0, len(sorted_arrayWithMessages_single_2D[index0] ) ):
        #        print( "sorted_arrayWithMessages_single_2D[" + str(index0) + "][" + str(index1) + "].positionInMultiMessage: " + str( sorted_arrayWithMessages_single_2D[index0][index1].positionInMultiMessage ) )
        #        print( "sorted_arrayWithMessages_single_2D[" + str(index0) + "][" + str(index1) + "].time: " + str( sorted_arrayWithMessages_single_2D[index0][index1].time ) )
        #        print( "sorted_arrayWithMessages_single_2D[" + str(index0) + "][" + str(index1) + "].content: " + str( sorted_arrayWithMessages_single_2D[index0][index1].content ) )
        #        print( "\n" )
        ###
        
        
        #now we have all singel-messages in one two-dimensional-array sorted_arrayWithMessages_single_2D[foo][bar], where all elements with the same [bar] are from the same multi-message. [foo] indicates different multi-messages
        
        # now we split it up in the different multi-messages:
        numberOfMultiMessages = -1
        for index in range( 0, len( sorted_arrayWithMessages_single_2D ) ):
            if numberOfMultiMessages < len( sorted_arrayWithMessages_single_2D[index] ):
                #print("numberOfMultiMessages < len( sorted_arrayWithMessages_single_2D[index] )")
                numberOfMultiMessages = len( sorted_arrayWithMessages_single_2D[index] )
        print( "numberOfMultiMessages: " + str( numberOfMultiMessages ) )
        
        
        arrayWithMessages_merged = []           #this will be our output-array. it will contain all messages (merged)
        for multiMessageIndex in range(0, numberOfMultiMessages):
            
            arrayWithMessages_merged.append( sorted_arrayWithMessages_single_2D[0][multiMessageIndex] )     #we push the first single message to the output-array. into that one, we will merge the other singleMessagers, to create a MultiMessage
            
            for index in range( 1, len( sorted_arrayWithMessages_single_2D ) ): # we do not start here with the zeroest-element, because we allready pushed that one to the output-array

                try:
                    arrayWithMessages_merged[multiMessageIndex].content += sorted_arrayWithMessages_single_2D[index][multiMessageIndex].content
                except IndexError:
                    print( "index error occured, but that can happen. multiMessageIndex: " + str(multiMessageIndex) + " ; index: " + str( index ) )    #this can 
                except Exception as exception:
                    print( "some other exception during multi-message-merge: exception: " + str( exception ) )

        #now everything sould be fine
        return arrayWithMessages_merged
        
        
        

    def getAllUsers(self):
        pathesToUsers = glob.glob(self.pathToUsers + "*")   #puts all pathes to all user-files into an array
        output = []
        for path in pathesToUsers:
            otherUser = user_class.User(path) #creates an user-object out of the path
            output.append(otherUser)
        return output       #returns the array with user-objects


    def interpretMessage(self, in_incommingMessage):
        ###
        # for system-notifications, it is necessary to have the current system-time
        systemTime = str(datetime.now().strftime('%H%M%S')) # we ceep it in that format, because it gets corrected in user.sendSMS()
        ###  
        #pdb.set_trace()
        
        # removing leading whitespaces:
        in_incommingMessage.content = in_incommingMessage.content.lstrip()
        
        #catch special cases:
        if in_incommingMessage.content == "mute":
            in_incommingMessage.content = "@@mute"       #because most people probalby won't accept the "@", i'm doing an exeption here. "mute" without an "@" is enough to mute, and will be handlet the same as "@mute"
        elif in_incommingMessage.content == "ping":
            in_incommingMessage.content = "@@ping"
        elif in_incommingMessage.content == "help":
            in_incommingMessage.content = "@@help"
        elif in_incommingMessage.content == "list":     # there will be two cases: the user who is sending "list" is in only one channel. in that case, no channel needs to be specified and the user doesn't need to add "@@". the other case: the user is in more then one channel. in that case, the user needs to send "@@list.channelname" and the "@@" will be neccessary. this line only affects the first case 
            in_incommingMessage.content = "@@list"
        elif in_incommingMessage.content == "channels":
            in_incommingMessage.content = "@@channels"
        elif in_incommingMessage.content == "":           #handle empty messages to avoid spam
            print("that was an empty message from: " + in_incommingMessage.fromNumber )
            return False
        elif in_incommingMessage.content == "unmute":
            in_incommingMessage.content = "@@unmute"

        if in_incommingMessage.content[0] == "@" and in_incommingMessage.content[1] == "@":
            #print("it is a command")
            self.interpretCommand( in_incommingMessage, systemTime )
        
        #if the message doesn't beginn with an "@@", 
        #then we just want to send the message to the channel, the user is in.
        #if the user is in more then one channel, this doesn't work and 
        #the user needs to get an sms about that fact
        
        else:
            #no pullMessage-request...
            #need to check, if the user is existing in database:
            sender = False
            allUsers = self.getAllUsers()       #getAllUser() returns a array with user-objects
            for user in allUsers:
                if in_incommingMessage.fromNumber == user.getNumber():
                    sender = user
            if sender != False:
                channelsSenderIsIn = sender.getChannels()
                if len(channelsSenderIsIn) == 1:                    # user is in one channel or in no channel
                    if channelsSenderIsIn[0] == "":                 #user is in no channel
                        if self.partyMode == False:
                            sender.sendSMS( "", "derDurchschlag", "you are in no channel. please send '@@join.channelName' to join a channel", systemTime, self.sendTimeStamp)
                        else:
                            print("received a message. sender is a existing user but is in no channel. sending him back the default answer")
                            self.sendDefaultMessageTo( in_incommingMessage.fromNumber )
                    else:                                           #user is only in one channel
                        for user in allUsers:
                            channelsOtherUserIsIn = user.getChannels()
                            if channelsSenderIsIn[0] in channelsOtherUserIsIn:
                                #pdb.set_trace()
                                if ( user.getNumber() != in_incommingMessage.fromNumber ) or self.senderGetsHisOwnMessage:        #sender doesn't need to get his own message
                                    time.sleep(4)   #it sometimes happend, that users get the same message twice, but this program only sent it once. trying if it is any better with this delay.
                                    user.sendSMS( channelsSenderIsIn[0], sender.getNick(), in_incommingMessage.content, in_incommingMessage.time, self.sendTimeStamp )
                                    #pdb.set_trace()
                if len( channelsSenderIsIn ) > 1:
                    sender.sendSMS( "", "derDurchschlag", "you are in more than one channel. you must specify the channel, you want to send your message to. do that by add '@@channelName' to the beginning of your message", systemTime, self.sendTimeStamp)
            else:
                print("received a message but doesn't begin with an '@@' and is not in database")
                if self.partyMode:
                    self.sendDefaultMessageTo(in_incommingMessage.fromNumber)
                    
    
    def getChannelInfoString(self, in_sender ):
        output = ""
        channels = in_sender.getChannels()
        for channel in channels:
            output += channel + ", "
        output = output[:-2]
        return output




    def interpretCommand(self, in_incommingMessage, in_systemTime ):     
        #pdb.set_trace() 
        commandBlock = in_incommingMessage.content.split(" ")[0]
        noMessageContent = False
        potentialText = ""
        try:
            potentialText = in_incommingMessage.content.split(" ", 1)[1]
        except IndexError:
            noMessageContent = True
        blocks = commandBlock.split(".")
        
        blocks[0] = blocks[0][1:]
        blocks[0] = blocks[0][1:]       #remove the "@@" from first block
        
        pathToAllExistingUsers = glob.glob(self.pathToUsers + "*")          #get pathes to all existing users
        #pdb.set_trace()
        NumbersOfAllExistingUsers = []
        senderIsAlreadyAUser = False
        for path in pathToAllExistingUsers:
            NumbersOfAllExistingUsers.append( path.split("/")[-1] )
        if in_incommingMessage.fromNumber in NumbersOfAllExistingUsers:
            #print("in_incommingMessage.fromNumber in NumbersOfAllExistingUsers. set senderIsAlreadyAUser = True")
            senderIsAlreadyAUser = True

        #try:
        if len(blocks) == 4:
            if ( blocks[0] == "hello" or blocks[0] == "hallo" ):
                
                if blocks[2] == "join":  #for example: @@hallo.myNickname.join.SomeChannelName
                    #print("blocks[2] == 'join'")
                    nick = blocks[1]
                    channels = []
                    reply = ""
                    if "@" in blocks[3]:
                        print("someone tryed to register and to join a channel at the same time but used an '@'-symbol in the title of the channel, which is not allowed")
                        channels.append("")             #adding no channel, because user has not joined yet
                        reply = "welcome to derDurchschlag, your friendly neighbourhood text-message-distributor. you just joined the system but tryed to join a channel with an '@'-symbol in it, wich is not allowed, so that failed... try again and use '@@join.channelName'. \nyou can always send '@@help' to get help."
                    else:
                        print("no '@'-symbol in channel-name")
                        if senderIsAlreadyAUser == False:
                            print("senderIsAlreadyAUser == False")
                            channels.append( blocks[3] )
                            reply = "welcome to derDurchschlag, your friendly neighbourhood text-message-distributor. you just joined the channel '" + blocks[3] + "'. you can always send '@@join.channelName' to join another channel or to establish one. and you can send '@@help' to get help."
                        else:
                            print("senderIsAlreadyAUser == True")
                            reply = "hello! you just re-join the sms-distributor-system 'derDurchschlag' (we already had you in our database). "
                            sender = user_class.User( self.pathToUsers + in_incommingMessage.fromNumber )
                            sender.joinChannel( blocks[3] )
                            oldNick = sender.nick
                            if oldNick != nick:
                                print( "setting sender.nick to " + nick )
                                reply += "your old nickname was '" + oldNick + "', your new one is '" + nick + "'. "
                                sender.nick = nick
                            sender.rewriteUserFile()
                            reply += "you currently are in these channels: " + self.getChannelInfoString( sender )
                    if senderIsAlreadyAUser == False:
                        self.writeNewUserFile( in_incommingMessage.fromNumber, nick, channels )
                        sender = user_class.User( self.pathToUsers + in_incommingMessage.fromNumber )
                    #else:
                    #    sender.rewriteUserFile()
                    sender.sendSMS( "", "", reply, in_systemTime,  False)
                else:
                    print("wrong syntax")
                    toSendString =  "echo 'it seems you used a wrong syntax. write: @@join.channelName' | gammu-smsd-inject TEXT " + in_incommingMessage.fromNumber 
                    os.system( toSendString )
            else:
                print("unknown command")
                toSendString =  "echo 'unknown command. if you want to register, send: @@hello.yourNickname.join.SomeChannel or @@hello.yourNickname and join a channel later' | gammu-smsd-inject TEXT " + in_incommingMessage.fromNumber 
                os.system( toSendString )


        if len(blocks) == 2:
            if ( blocks[0] == "hello" or blocks[0] == "hallo" ):    #for example: @@hello.myNickname
                print( "blocks[0] == 'hello' or blocks[0] == 'hallo'" )
                if senderIsAlreadyAUser == False:         
                    print( "senderIsAlreadyAUser == False" )              
                    nick = blocks[1]
                    channels = []
                    channels.append("")             #adding no channel, because user has not joined yet
                    self.writeNewUserFile( in_incommingMessage.fromNumber, nick, channels )
                    sender = user_class.User( self.pathToUsers + in_incommingMessage.fromNumber )
                    sender.sendSMS( "", "derDurchschlag", "welcome to derDurchschlag, your friendly neighbourhood text-message-distributor. send '@@join.channelName' to join a channel or send '@@help' to get help.", in_systemTime, self.sendTimeStamp)
                else:                       # senderIsAlreadyAUser == True
                    #user is already an existing user
                    print( "senderIsAlreadyAUser == True" )  
                    sender = user_class.User( self.pathToUsers + in_incommingMessage.fromNumber )
                    reply = "you are already an existing user. if you want to join a channel, just send '@@join.channelName'. "
                    nick = blocks[1]
                    oldNick = sender.nick
                    print("nick: " + nick)
                    print("oldNick: " + oldNick)
                    if oldNick != nick:
                        print( "setting sender.nick to " + nick )
                        sender.nick = nick
                        reply += "we changed your nickname from '" + oldNick + "' to '" + sender.nick + "'. "
                    sender.rewriteUserFile()
                    reply += "you currently are in these channels: "
                    reply += self.getChannelInfoString( sender )
                    sender.sendSMS( "", "derDurchschlag", reply, in_systemTime, self.sendTimeStamp)
            
            elif blocks[0] == "selftest":
                #print("selftest received")
                self.receivedSelftestsArray.append( blocks[1] )
                print("self.receivedSelftestsArray: " + str(self.receivedSelftestsArray) + "\n\n" )
                self.timeLastSelfTestHasBeenReceived = datetime.now()
                self.adminIsInformed = False
            
            elif blocks[0] == "list" or blocks[1] == "list":                               #for example @@list.myChannel or @@myChannel.list
                #pdb.set_trace()
                if senderIsAlreadyAUser:
                    requestedChannel = blocks[1]
                    
                    if blocks[1] == "list":             #just in case the user switched the command-keyword ('list) and the channel-name
                        requestedChannel = blocks[0]
                        
                    print("list for requestedChannel: " + str(requestedChannel))
                    sender = user_class.User( self.pathToUsers + in_incommingMessage.fromNumber ) 
                    
                    ###below is the solution, if we don't want to allow the listing of channels, the user is not in. but actually..why not?
                    #if requestedChannel in sender.getChannels():
                    #    pass
                    #    #missing: sending answer...
                    #else:
                    #    sender.sendSMS( "", "derDurchschlag", "you tried to list the users in channel '" + str(requestedChannel) + "' but you have to join that channel first. send '@@join." + str(requestedChannel) + "'", in_systemTime, self.sendTimeStamp )
                    
                    usersInSameChannel = []
                    for path in pathToAllExistingUsers:
                        #print("path: " + str(path))
                        user = user_class.User( path )
                        channelsUserIsIn = user.getChannels()
                        #print("channelsUserIsIn: " + str(channelsUserIsIn))
                        if requestedChannel in channelsUserIsIn:
                            #print("user is in requested channel")
                            usersInSameChannel.append(user)
                    stringWithUsersInChannel = ""
                    for user in usersInSameChannel:
                        stringWithUsersInChannel += ", " + user.nick
                        if user.getMuted():
                            stringWithUsersInChannel += "(muted)"
                    stringWithUsersInChannel = stringWithUsersInChannel[2:] #removing the first seperator ( ", user1, user2, user3" -> "user1, user2, user3" )
                    sender.sendSMS( "", "derDurchschlag", "users in channel '" + str(requestedChannel) + "': \n" + stringWithUsersInChannel, in_systemTime, False)
                else:
                    print("someone tryed to list all users in channel " + str( blocks[1] ) + " but is not even registered")
            elif blocks[0] == "exit" or blocks[1] == "exit":        #for example @@exit.myChannel or @@myChannel.exit
                if senderIsAlreadyAUser:
                    channelToLeave = ""
                    if blocks[0] == "exit":
                        channelToLeave = blocks[1]
                    elif blocks[1] == "exit":
                        channelToLeave = blocks[0]
                    else:
                        print("something went wrong")
                        exit(0)
                    temp_path = self.pathToUsers + in_incommingMessage.fromNumber
                    sender = user_class.User( temp_path )
                    if sender.exitChannel( channelToLeave ) == True:
                        sender.sendSMS( "", "derDurchschlag", "you left the channel '" + channelToLeave + "'", in_systemTime, self.sendTimeStamp )
                    else:
                        sender.sendSMS( "", "derDurchschlag", "you tryed to leave channel '" + channelToLeave + "' but you are not even in that channel", in_systemTime, self.sendTimeStamp  )
                else:
                    print("someone tryed to leave a channel but is not even registered")

            elif blocks[0] == "join":          #for example: @@join.someChannel
                if senderIsAlreadyAUser == True:
                    UserFilePath = self.pathToUsers + in_incommingMessage.fromNumber
                    #pdb.set_trace()
                    sender = user_class.User( UserFilePath )
                    if "@" in blocks[1]:
                        print("user tryed to join a channel with a '@' in it, which is not allowed")
                        sender.sendSMS("", "derDurchschlag", "you tryed to join a channel with a '@'-symbol in the title, which is not allowed. try again and use this syntax to join a channel: @@join.channelName", in_systemTime, False )
                    elif blocks[1] in self.forbiddenChannelnames:
                        print("user tryed to establish a channelname which is not allowed!")
                        sender.sendSMS( "", "derDurchschalg", "you tryed to establish a channel with the title '" + str( blocks[1] ) + "'. this is not allowed since the word is needed to execute commands on the system. please use another name and try again. you can always send '@@help' to get help", in_systemTime, False )
                    else:
                        successfullyJoined = sender.joinChannel( blocks[1] )
                        if successfullyJoined:
                            sender.sendSMS( "", "derDurchschlag", "you joined the channel '" + blocks[1] + "'. cu!", in_systemTime, self.sendTimeStamp )
                        else:
                            sender.sendSMS( "", "derDurchschlag", "it seems, you are already in the channel '" + blocks[1] + "'", in_systemTime, self.sendTimeStamp )
                else:
                    #user tryed to join a channel but is not even an existing user...
                    toSendString =  "echo 'derDurchschlag: you tryed to join a channel but you need to join the system first. send '@@hello.yourNickname.join.someChannel' to do this' | gammu-smsd-inject TEXT " + in_incommingMessage.fromNumber 
                    os.system( toSendString )
            
            else:
                print("two blocks, but unknown command!")

                toSendString =  'echo ' + "() derDurchschlag: sorry, you tryed execute a command, but i could not understand what you mean. use '@@help' to get a overview with all commands" + ' | gammu-smsd-inject TEXT ' + in_incommingMessage.fromNumber + ' -len ' + str( len(self.pullmessageAnswerArray[in_incommingMessage.answerIndex]) )
                print("toSendString: " + str( toSendString ))
                os.system( toSendString )
                
                
                
                

        elif len( blocks ) == 1:
            if blocks[0] == "ping":                               # @@ping
                print( "sending a pong to: " + in_incommingMessage.fromNumber )
                toSendString =  "echo pong | gammu-smsd-inject TEXT " + in_incommingMessage.fromNumber 
                os.system( toSendString )
            elif blocks[0] == "help":
                print( "received a help-command" )
                toSendString = "echo '" + str( self.helpText.replace("'", '"') ) + "' | gammu-smsd-inject TEXT " + in_incommingMessage.fromNumber + " -len " + str( len( self.helpText ) )
                #pdb.set_trace()
                os.system( toSendString )
                  
            
                
            elif blocks[0] == "list":                           # @@list
                if senderIsAlreadyAUser:
                    UserFilePath = self.pathToUsers + in_incommingMessage.fromNumber
                    sender = user_class.User( UserFilePath )
                    channels = sender.getChannels()
                    if len( channels ) > 1:
                        print("user tryed to list all users in his channel, but is in more than one")
                        #toSendString =  "echo 'derDurchschlag: you tryed to list all users in your channel but you are in more than one. send @@list.someChannel to specify the channel' | gammu-smsd-inject TEXT " + in_incommingMessage.fromNumber 
                        #os.system( toSendString )
                        sender.sendSMS( "", "derDurchschlag", "you tryed to list all users in your channel but you are in more than one. send '@@list.someChannel' to specify the channel", in_systemTime, False )
                    elif len( channels ) == 0:
                        print("user tryed to list all users in his channel, but is in no channel")
                        toSendString =  "echo 'derDurchschlag: you tryed to list all users in your channel but you are in no channel. send @@join.someChannel join a channel' | gammu-smsd-inject TEXT " + in_incommingMessage.fromNumber 
                        os.system( toSendString )
                    else:
                        ###below is the solution, if we don't want to allow the listing of channels, the user is not in. but actually..why not?
                        #if requestedChannel in sender.getChannels():
                        #    pass
                        #    #missing: sending answer...
                        #else:
                        #    sender.sendSMS( "", "derDurchschlag", "you tried to list the users in channel '" + str(requestedChannel) + "' but you have to join that channel first. send '@@join." + str(requestedChannel) + "'", in_systemTime, self.sendTimeStamp )
                        requestedChannel = channels[0]
                        usersInSameChannel = []
                        for path in pathToAllExistingUsers:
                            user = user_class.User( path )
                            channelsUserIsIn = user.getChannels()
                            if requestedChannel in channelsUserIsIn:
                                usersInSameChannel.append(user)
                        stringWithUsersInChannel = ""
                        for user in usersInSameChannel:
                            stringWithUsersInChannel += ", " + user.nick
                            if user.getMuted():
                                stringWithUsersInChannel += "(muted)"
                        stringWithUsersInChannel = stringWithUsersInChannel[2:] #removing the first seperator ( ", user1, user2, user3" -> "user1, user2, user3" )
                        sender.sendSMS( "", "derDurchschlag", "users in channel '" + str(requestedChannel) + "': \n" + stringWithUsersInChannel, in_systemTime, False)
                        #pdb.set_trace()
                else:
                    print("someone tryed to list all users in his channel, but is no even a user")
                    toSendString =  "echo 'derDurchschlag: you tryed to list all users in your channel but you need to join the system first. send @@hello.yourNickname.join.someChannel to do so' | gammu-smsd-inject TEXT " + in_incommingMessage.fromNumber 
                    os.system( toSendString )
            elif blocks[0] == "channels":
                if senderIsAlreadyAUser:
                    UserFilePath = self.pathToUsers + in_incommingMessage.fromNumber
                    sender = user_class.User( UserFilePath )
                    channels = sender.getChannels()
                    if len( channels ) == 0:
                        sender.sendSMS( "", "derDurchschlag", "you are in no channel. send '@@join.someChannel' to join a channel", in_systemTime, False )
                    else:
                        channelString = self.getChannelInfoString( sender )
                        muteInfo = ""
                        if sender.getMuted():
                            muteInfo = "\nyou are currently muted until: "
                            muteInfo += sender.getMuteUntil()
                        sender.sendSMS( "", "derDurchschlag", "you are in these channels: \n" + channelString + muteInfo, in_systemTime, False )
                else:
                    print("someone tryed to list his channels, but is not even a registered user")
                    toSendString = "echo 'derDurchschlag: you tryed to list your channels but you need to join the system first. send @@hello.yourNickname.join.someChannel to do this' | gammu-smsd-inject TEXT " + in_incommingMessage.fromNumber 
                    os.system( toSendString )
            elif blocks[0] == "mute":                           # @@mute or just mute
                if senderIsAlreadyAUser == True:
                    UserFilePath = self.pathToUsers + in_incommingMessage.fromNumber
                    sender = user_class.User( UserFilePath )
                    sender.mute()
                else:
                    #user tryed to mute not even an existing user...
                    print("user tryed to mute not even an existing user...")
                    toSendString =  "echo 'derDurchschlag: you tryed to mute a channel but you need to join the system first. send @@hello.yourNickname.join.someChannel to do this' | gammu-smsd-inject TEXT " + in_incommingMessage.fromNumber 
                    os.system( toSendString )
            elif blocks[0] == "unmute":                         # @@unmute
                if senderIsAlreadyAUser == True:
                    UserFilePath = self.pathToUsers + in_incommingMessage.fromNumber
                    sender = user_class.User( UserFilePath )
                    sender.unmute()
                else:
                    print("user tryed to unmute not even an existing user...")
                    toSendString =  "echo 'derDurchschlag: you tryed to unmute a channel but you need to join the system first. send @@hello.yourNickname.join.someChannel to do this' | gammu-smsd-inject TEXT " + in_incommingMessage.fromNumber 
                    os.system( toSendString )
            else:
                if len(blocks) == 1 and senderIsAlreadyAUser == True:                        #command only consists of one word. for example '@@mensen'. then its clear, it is a channel name (since none of the command-keywords could be detected.
                    channelName = blocks[0].split(" ")[0]
                    messageHasBeenSend = False                  #message has been send at least once. if not, we can write the user back, that the channel is empty
                    allUsers = self.getAllUsers()
                    #pdb.set_trace()
                    sender = user_class.User( self.pathToUsers + in_incommingMessage.fromNumber )
                    channelsSenderIsIn = sender.getChannels()
                    if channelName in channelsSenderIsIn:    #checking if sender is in that channel
                        for otherUser in allUsers:
                            channelsOtherUserIsIn = otherUser.getChannels()
                            if channelName in channelsOtherUserIsIn:
                                if otherUser.getNumber() != in_incommingMessage.fromNumber or self.senderGetsHisOwnMessage:          #the sender doesn't need to get his own message
                                    #pdb.set_trace()
                                    otherUser.sendSMS( channelName, sender.getNick(), potentialText, in_incommingMessage.time, self.sendTimeStamp )
                                    messageHasBeenSend = True
                        if messageHasBeenSend == False:
                            toSendString = "you tryed to send a message to the channel '" + str( channelName ) + "' but it seems, you are the only one in that channel. make sure, the channel-name is not missspelled or join another channel with more party-people. or contact your admin. or send '@@help' to get help-information."
                            sender.sendSMS("", "derDurchschlag", toSendString, in_systemTime, self.sendTimeStamp)
                    else:
                        toSendString = "your tryed to send a message to channel '" + str(channelName) + "' but you are not even in that channel. please send '@@join." + str(channelName) + "' to join this channel. or maybe you tryed to execute a command (like @@mute, or @@ping) and you misspelled it. please send '@@help' to get a list with all commands."
                        sender.sendSMS("", "derDurchschlag", toSendString, in_systemTime, self.sendTimeStamp)
                else:
                    #user tryed to send a message to a channel, but is not even an existing user
                    print("user tryed to send a message to a channel, but is needs to register first, or used wrong syntax")

        
       

    def writeNewUserFile( self, in_number, in_nick, in_channels):
        print("entering writeUserFile")
        userFilePath = self.pathToUsers + in_number
        userFile = open( userFilePath, 'a')
        userFile.write( "nick: " + in_nick + "\n" )
        userFile.write( "channels: " + in_channels[0] )
        if len(in_channels) > 1:
            for index in range( 1, len(in_channels) ):
                userFile.write( "," + in_channels[index] )
        userFile.write("\n")
        userFile.write("mutedUntil: \n")
        userFile.close()



    def handlePullMessageRequest( self, in_incommingMessage ):
        toSendString =  'echo ' + self.pullmessageAnswerArray[in_incommingMessage.answerIndex] + ' | gammu-smsd-inject TEXT ' + in_incommingMessage.fromNumber + ' -len ' + str( len(self.pullmessageAnswerArray[in_incommingMessage.answerIndex]) )
        print("handlePullMessageRequest(): toSendString: " + toSendString )
        os.system( toSendString )
        
        
    def sendDefaultMessageTo(self, in_number):
        toSendString = 'echo ' + self.pullmessageDefaultAnswer + ' | gammu-smsd-inject TEXT ' + in_number + ' -len ' + str( len(self.pullmessageDefaultAnswer) )
        print("sendDefaultMessageTo(): toSendString: " + toSendString) 
        os.system(toSendString)
